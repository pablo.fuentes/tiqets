# Test assignment reply

## Run app 

We have 3 ways of getting this application:

### Running directly on your local machine: 

Note: having a [virtual env](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) 
is highly recommended 

1. Install all dependencies ` pip install -r requirements.txt `
2 Run the app `python app/app.py`

Note: It is a good practice if we set the environment variables before running (if you dont have them as environment
variables ), so the best is to run `APP_AES_KEY=<YOUR_AWESOME_KEY> PORT=5000 python app/app.py` 
`
### Running with docker

Important: if you want to download this container, you should be logged in in gitlab.com registry, follow
 [this instructions](https://gitlab.com/help/user/packages/container_registry/index#authenticating-to-the-gitlab-container-registry)
to log in the registry

And then just run it with `docker run registry.gitlab.com/pablo.fuentes/tiqets:latest `
I will suggest to add your ports and environment variables to the container (but you can run it without adding them),
 so the best way to run it would be 
`docker run -e APP_AES_KEY=<YOUR_AWESOME_KEY> -e PORT=5000  -p 5001:5000 registry.gitlab.com/pablo.fuentes/tiqets:latest` 
 
 
 ## Kubernetes deployment 
 
 Note: Fort his solution I will assume you have running minikube and your kubectl context is pointing to it
 
 This app is ready to be ran in kubernetes, but first we will have to create the config files inside k8s
 
 First of all kubernetes ahs two useful types of objects in this case, secrets (we will save our aes key here) and
  config (for host and port)
 
 To create the secret, just type ` kubectl create secret generic aes-keys --from-literal=app-key=<YOUR_AWESOME_KEY>`, 
 Feel free to use a secret file instead of this but careful with it, we will store some sensitive data here
 
 I have Created a config.yml file in the deployment folder with the needed values, feel free to edit them if needed.
 To save them type `kubectl apply -f deployment/config.yml`
 
 To run the application then, apply the deployment file typing 
 
`kubectl apply -f deployment/deployment.yml`

Ti test it, the best way is using services. run `kubectl apply -f deplyment/service.yml` and then 
`minikube service web-app --url`


## Improvements

1. To run the application ub a production-like grade, we should run it with [gunicorn](https://gunicorn.org/). 
This will allow us to have a much better performance

   git commit  -> 86d35b7ff344bb540372f6446650aaf891c31931

2. I have overwritten the server header. This header could reveal potential attackers possible errors/bugs 
that can be exploited

3. In the kubernetes era, having our service storing our logs into a file is an antipatern.  I have removed that 
configuration, so logs are being throwed to the stdout. inside the kubernetes we should have another pod (a daemonset) 
that should recollect all of them. Examples of this could be [Logstash](https://www.elastic.co/logstash) or 
[dd-agent](https://docs.datadoghq.com/agent/)

4. Implementing jwt/oauth any login system. If anyone can encrypt/decrypt data from our data, do we really have to encrypt it? 

   [comment]: <> (IMPLEMENTAR todo)

5. Container scanning: implementing a container scanning in our cicd system will help us to have containers with known
    vulnerabilities (and available fixes) gitlab has a template that using spublic template
     (Container-Scanning.gitlab-ci.yml) helps us with this. it uses [Clair](https://github.com/quay/clair) to help us 
     with it. Unfortunately, this is a premium feature.

6. Also we should implement others for our code itself and dependencies. [safety](https://pyup.io/safety/) is an 
   example of this
   
    I have used SonarQube  with SonarCloud. SonarQube is an open source platform for continuous inspection of quality
    in our code, So we can prevent bugs and get reports of a lot of things like testing or duplicated code. it also 
    gives us possible locations to search for security problems. Ín this case, sonar has warned us about logger's        configuration and the encryption method
    
    project url -> [https://sonarcloud.io/dashboard?id=pablo.fuentes_tiqets]()
    
    git commit -> 49523c520964407dd017182b5cac7f96854589d8

7. Add tests. Testing is the only way to make sure that everything could be automatically released to production 
   safely. unit tests for our functions and acceptance ones for the hole app are the best way to be 100% sure that 
   everything will work as expected in production 

8. HTTPS. Working with kubernetes and cloud systems, we don  have to worry much about this at the app level. the ingress
  or the load balancer will handle this for us

9. I have left some values as default for our environment variables. This is no the best way to work (I think process
 should crash, like when we try to create a pod but the env in the deployment is not in the namespace) but i lost an 
 hour because I did not read the readme ...
 
10. Following the tips given by SonasQube, i had a look to our encryption method and the logger. the logger seems to 
be fine (We should take care of who has access to kubetcl/logs manager) but it is fine. Maybe the issue could be with
pycrypto, witch hasn't been updated for the last 6 years.... i don't know if this is fine but is totally out of the 
scope of this  assignment 

11. Resource quotas. It is important to have well defined resources limits/usages in our deployment. With this we will
 be able  to handle horizontal autoscalling witch in a peak of traffic can be the key of using k8s. One important note here, 
 keep in mind that the ones that i have in the repo are not the ones that i think are "ideal" this is becouse the remote
 k8s that i am using have got less resources that the ones that i would like. In the assignment attached i will include 
 the ones I think that fits much better in our app 

Note: i have decided to left one worker (or as much as you inject via env) in gunicorn because in k8s, we should work 
with pods and autoscalling, using as much threads as the machine has (or more than that) is an anti pattern (in k8s) and
could become in problems with nodes and scheduling 
 
Super important note: As you can see in the deployment file in my repo livenessProve and readinessProve are commented.
This is because an issue in the cluster that I am already using on remote. If you uncomment them (I will send this
 uncommented) and try to deploy in minikube this proves are working