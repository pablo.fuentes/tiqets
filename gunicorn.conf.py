import multiprocessing
import os

port = os.getenv('PORT')
if port is None:
    port = "5000"

host = os.getenv('HOST')
if host is None:
    host = "0.0.0.0"

workers = os.getenv('WORKERS')
if workers is None:
    workers = 1

bind = host + ":" + port
