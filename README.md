# Devops Test 01

## Containerize an application

This repository contains a very simple application written in Python that encrypts and decrypts secrets.

### Instructions for running the app

```
> pip install -r requirements.txt

> FLASK_APP=app.py APP_AES_KEY=somekey234876283 flask run
```

Once it's running you can test the endpoints with

```
 curl -X POST  http://localhost:5000/login  -H 'Content-Type: application/json' -d '{"username": "a", "password": "a"}'
```

and then, with the given token
```
curl -X POST  http://192.168.99.100:31277/encrypt   -H 'Authorization: Bearer <YOUR_ACCESS_TOKEN>' -d hello!

```

Environment variables you can use:

APP_AES_KEY -> The aes key to encrypt/decrypt your data
PORT -> Port where the app runs
HOST -> Ip where the app will accept connections
WORKERS -> (only if running with gunicorn) number of gunicorn workers


Your goal is to improve this application to be able to run in kubernetes.

## Deliverables

- New files and modifications to the application to be able to run in Minikube (Linux) or Docker for Mac (macOS)
- Instructions on how to run the application
- BONUS: configure GitLab CI

## Notes

- Don't spend more than 2 hours on this assignment.
- Think about logging and monitoring. Suggest and implement improvement.
- Security is also our priority. What should be improved or changed? If you can, implement the changes.
- You should explain, why you made a specific technical decision.
