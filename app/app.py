from flask import Flask, request, jsonify
from Crypto.Cipher import AES
from Crypto import Random
from flask_jwt_extended import JWTManager, jwt_required, create_access_token

import base64
import os
import logging

logging.basicConfig(level=logging.INFO)

log = logging.getLogger()

encrypt_key = os.getenv('APP_AES_KEY')

if encrypt_key is None:
    log.warn("Missing encrypt key, using default one")
    encrypt_key = "test-app-aes-key"

jwt_secret = os.getenv('TOKEN_SECRET_KEY')

if jwt_secret is None:
    log.warn("Missing jwt key, using default one")
    jwt_secret = "super-secret"

app = Flask(__name__)

BS = 16

app.config['JWT_SECRET_KEY'] = jwt_secret

jwt = JWTManager(app)


@app.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username:
        return jsonify({"msg": "Missing username parameter"}), 400
    if not password:
        return jsonify({"msg": "Missing password parameter"}), 400

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200


def pad(s): return s + (BS - len(s) % BS) * chr(BS - len(s) % BS)


def unpad(s): return s[:-ord(s[len(s) - 1:])]


@jwt_required
def encrypt(raw):
    raw = pad(raw)
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(encrypt_key, AES.MODE_CBC, iv)
    return base64.b64encode(iv + cipher.encrypt(raw))


@jwt_required
def decrypt(enc):
    enc = base64.b64decode(enc)
    iv = enc[:16]
    cipher = AES.new(encrypt_key, AES.MODE_CBC, iv)
    return unpad(cipher.decrypt(enc[16:]))


@app.route('/encrypt', methods=['POST'])
def encrypt_route():
    body = request.data.decode()
    log.info(f"Encrypting {body}")
    return encrypt(body)


@app.route('/decrypt', methods=['POST'])
def decrypt_route():
    """todo poner nota de seguridad en doc 2"""
    body = request.data.decode()
    log.info(f"Decrypting {body}")
    return decrypt(body)


@app.after_request
def after_request(response):
    response.headers["server"] = 'Web-app'
    return response


@app.route('/readiness', methods=['GET'])
def readiness_route():
    return 'alive'


@app.route('/liveness', methods=['GET'])
def liveness_route():
    return 'alive'


if __name__ == '__main__':

    port = os.getenv('PORT')
    if port is None:
        log.warn("Missing port, using default one")
        port = 5000

    host = os.getenv('HOST')
    if port is None:
        log.warn("Missing host, using default one")
        host = "0.0.0.0"

    app.run(debug=True, host=host, port=port)
