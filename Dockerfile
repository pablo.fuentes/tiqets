FROM python:3.7-alpine

RUN apk add gcc g++ make libffi openssl

RUN addgroup -S python && adduser -S python -G python

ADD requirements.txt .

RUN pip install gunicorn

ADD wsgi.py  .

ADD gunicorn.conf.py .

ADD app/ app/

RUN  pip install -r requirements.txt

USER python

CMD [ "gunicorn", "-c", "gunicorn.conf.py", "wsgi:app"]